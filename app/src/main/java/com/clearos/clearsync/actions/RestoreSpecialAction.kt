/*
 * OAndBackupX: open-source apps backup and restore app.
 * Copyright (C) 2020  Antonios Hazim
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.clearos.clearsync.actions

import android.content.Context
import android.net.Uri
import com.clearos.clearsync.handler.ShellHandler
import com.clearos.clearsync.handler.ShellHandler.Companion.quote
import com.clearos.clearsync.handler.ShellHandler.Companion.runAsRoot
import com.clearos.clearsync.handler.ShellHandler.Companion.utilBoxQuoted
import com.clearos.clearsync.handler.ShellHandler.ShellCommandFailedException
import com.clearos.clearsync.items.AppInfo
import com.clearos.clearsync.items.BackupProperties
import com.clearos.clearsync.items.SpecialAppMetaInfo
import com.clearos.clearsync.items.StorageFile
import com.clearos.clearsync.items.StorageFile.Companion.fromUri
import com.clearos.clearsync.utils.CryptoSetupException
import com.clearos.clearsync.utils.isEncryptionEnabled
import com.clearos.clearsync.utils.suUncompressTo
import org.apache.commons.io.FileUtils
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

class RestoreSpecialAction(context: Context, shell: ShellHandler) : RestoreAppAction(context, shell) {

    @Throws(CryptoSetupException::class, RestoreFailedException::class)
    override fun restoreAllData(app: AppInfo, backupProperties: BackupProperties, backupLocation: Uri, backupMode: Int) {
        restoreData(app, backupProperties, fromUri(context, backupLocation))
    }

    @Throws(RestoreFailedException::class, CryptoSetupException::class)
    override fun restoreData(app: AppInfo, backupProperties: BackupProperties, backupLocation: StorageFile) {
        Timber.i(String.format("%s: Restore special data", app))
        val metaInfo = app.appMetaInfo as SpecialAppMetaInfo
        val tempPath = File(context.cacheDir, backupProperties.packageName ?: "")
        val isEncrypted = context.isEncryptionEnabled()
        val backupArchiveFilename = getBackupArchiveFilename(BACKUP_DIR_DATA, isEncrypted)
        val backupArchiveFile = backupLocation.findFile(backupArchiveFilename)
                ?: throw RestoreFailedException("Backup archive at $backupArchiveFilename is missing")
        try {
            openArchiveFile(backupArchiveFile.uri, isEncrypted).use { archive ->
                tempPath.mkdir()
                // Extract the contents to a temporary directory
                archive.suUncompressTo(tempPath)

                // check if all expected files are there
                val filesInBackup = tempPath.listFiles()
                val expectedFiles = metaInfo.fileList
                        .map { pathname: String? -> File(pathname ?: "") }
                        .toTypedArray()
                if (filesInBackup != null && (filesInBackup.size != expectedFiles.size || !areBasefilesSubsetOf(expectedFiles, filesInBackup))) {
                    val errorMessage = "$app: Backup is missing files. Found $filesInBackup; needed: $expectedFiles"
                    Timber.e(errorMessage)
                    throw RestoreFailedException(errorMessage, null)
                }
                val commands = mutableListOf<String>()
                for (restoreFile in expectedFiles) {
                    commands.add("$utilBoxQuoted mv ${quote(File(tempPath, restoreFile.name))} ${quote(restoreFile)}")
                }
                val command = commands.joinToString(" ; ")  // no dependency
                runAsRoot(command)
            }
        } catch (e: ShellCommandFailedException) {
            val error = extractErrorMessage(e.shellResult)
            Timber.e("$app: Restore $BACKUP_DIR_DATA failed. System might be inconsistent: $error")
            throw RestoreFailedException(error, e)
        } catch (e: FileNotFoundException) {
            throw RestoreFailedException("Could not find backup archive", e)
        } catch (e: IOException) {
            Timber.e("$app: Restore $BACKUP_DIR_DATA failed with IOException. System might be inconsistent: $e")
            throw RestoreFailedException("IOException", e)
        } finally {
            val backupDeleted = FileUtils.deleteQuietly(tempPath)
            Timber.d("$app: Uncompressed $BACKUP_DIR_DATA was deleted: $backupDeleted")
        }
    }

    override fun restorePackage(backupLocation: Uri, backupProperties: BackupProperties) {
        // stub
    }

    override fun restoreDeviceProtectedData(app: AppInfo, backupProperties: BackupProperties, backupLocation: StorageFile) {
        // stub
    }

    override fun restoreExternalData(app: AppInfo, backupProperties: BackupProperties, backupLocation: StorageFile) {
        // stub
    }

    override fun restoreObbData(app: AppInfo, backupProperties: BackupProperties?, backupLocation: StorageFile) {
        // stub
    }

    companion object {
        private fun areBasefilesSubsetOf(set: Array<File>, subsetList: Array<File>): Boolean {
            val baseCollection: Collection<String> = set.map { obj: File -> obj.name }.toHashSet()
            val subsetCollection: Collection<String> = subsetList.map { obj: File -> obj.name }.toHashSet()
            return baseCollection.containsAll(subsetCollection)
        }
    }
}